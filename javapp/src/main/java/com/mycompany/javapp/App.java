package com.mycompany.javapp;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.sound.sampled.LineUnavailableException;

public class App {
    public static void main(String[] args) throws LineUnavailableException {
        String date = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date());
        Recorder.recording(new Recorder("./records/audio/" + date + "_enregistrement.wav"), date + "_enregistrement.wav");
    }
}
