package com.mycompany.javapp;

import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.WebSocketContainer;


/** Sample audio recorder */
public class Recorder extends Thread {
    private final TargetDataLine line;
    private final AudioFileFormat.Type targetType = AudioFileFormat.Type.WAVE;

    private final AudioInputStream inputStream;
    private File file;
    
    private static WebsocketEndpoint app;    

    static GraphicsConfiguration gc;
    private static JButton btnStart;
    private static JButton btnStop;
    private static JPanel panneau;

    public Recorder(String outputFilename) throws LineUnavailableException {
        AudioFormat audioFormat = new AudioFormat (
            AudioFormat.Encoding.PCM_SIGNED, // Encoding technique
            44100.0F, // Sample Rate
            16, // Number of bits in each channel
            2, // Number of channels (2=stereo)
            4, // Number of bytes in each frame
            44100.0F, // Number of frames per second
            false // Big-endian (true) or little-endian (false)
        );

        DataLine.Info info = new DataLine.Info(TargetDataLine.class, audioFormat);
        line = (TargetDataLine) AudioSystem.getLine(info);
        line.open(audioFormat);
        inputStream = new AudioInputStream(line);
        file = new File(outputFilename);
    }

    Recorder() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void startRecording() {
        line.start();
        start();
    }

    public void stopRecording() {
        line.stop();
        line.close();
    }

    public void run() {
        try { AudioSystem.write(inputStream,targetType,file); }
        catch (IOException e) { e.printStackTrace(); }
    }

    public String getPath() { return file.getAbsolutePath(); }

    public static void pressEnter(String text) {
        System.out.println(text);
        new Scanner(System.in).nextLine();
    }

    public static void recording(Recorder r, String filename) {

        JFrame window= new JFrame(gc);
        window.setTitle("Reconnaissance vocale - VocalCorp");
        window.setSize(400, 200);
        window.setLocationRelativeTo(null);
        window.setResizable(false);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        btnStart = new JButton("Démarrer l'enregisrement");
        btnStart.setBackground(Color.decode("#69b064"));
        btnStop = new JButton("Arrêter l'enregistrement");
        btnStop.setBackground(Color.decode("#db5346"));
        btnStop.setVisible(false);

        //MouseListener
        btnStart.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                btnStart.setVisible(false);
                btnStop.setVisible(true);
                r.startRecording();
            }
        });

        btnStop.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                btnStart.setVisible(true);
                btnStop.setVisible(false);
                r.stopRecording();
                window.dispose();                
             
                WebSocketContainer container = ContainerProvider.getWebSocketContainer();
                WebsocketEndpoint app = new WebsocketEndpoint();
                try {
                    container.connectToServer(app, new URI("ws://localhost:8080/projectApp/hello"));
                    app.sendMessage(r.getPath());
                } catch (URISyntaxException ex) {
                    Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                } catch (DeploymentException ex) {
                    Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        //Création d'un JPanel (conteneur intermédiaire)
        panneau = new JPanel();
        panneau.setLayout(new GridBagLayout());

        panneau.add(btnStart);
        panneau.add(Box.createRigidArea(new Dimension(10, 0)));
        panneau.add(btnStop);

        //Ajout du conteneur inter dans le conteneur de base
        window.getContentPane().add(panneau);

        //Rends la fenêtre visible
        window.setVisible(true);
    }

}
