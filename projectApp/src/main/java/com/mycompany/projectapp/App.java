package com.mycompany.projectapp;

import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;

@ServerEndpoint("/hello")
public class App {

    private Session session;

    @OnOpen
    public void onCreateSession(Session session){
        this.session = session;
    }

    @OnMessage
    public void onMessage(String m) throws IOException {
        for (Session session : this.session.getOpenSessions()) {
            session.getBasicRemote().sendText(m);
        }
        System.out.println("Message = " + m);
    }
}
