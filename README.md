# VocalCorp - Projet Informatique

Projet Informatique n°3 - ING 1 - Institut G4

## Structure du projet

Le projet est composé de deux sous-projets :

- projectApp contient le serveur avec la page web
- javapp contient l'application Java

## Lancement du projet

Nous vous conseillons d'utiliser l'IDE NetBeans associé à un serveur GlassFish 5.1.0.

Vous devez lancer projectApp dans un premier temps, pour pouvoir lancer javapp